function err = linf_pricerr(dpricerr, mdl)

    err = norm(dpricerr, Inf);
end
