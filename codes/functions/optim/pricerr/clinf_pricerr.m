function err = clinf_pricerr(dpricerr, mdl)

    err = norm(dpricerr, Inf);
end
