function err = l2_pricerr(dpricerr, mdl)

    err = norm(dpricerr, 2);
end
