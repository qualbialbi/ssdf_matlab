function err = cl1_pricerr(dpricerr, mdl)

    err = norm(dpricerr, 1);
end
