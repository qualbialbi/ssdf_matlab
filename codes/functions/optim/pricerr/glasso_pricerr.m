function err = glasso_pricerr(dpricerr, mdl)

    err = 0.0;    
    for g = 1:mdl.dgroups(end)
        %err = err + norm( dpricerr(mdl.dgroups == g) , 2 );
        err = err + norm( mdl.sqrVinv{g} * dpricerr(mdl.dgroups == g) , 2 );
    end

end
