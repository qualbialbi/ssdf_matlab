function disp = hell_ssdf_disp(m, mdl)
    
    if any( m < 0 )
        disp = Inf;
    else
        disp = 0.5 * mdl.Tinv * sum(m.^0.5);
    end
end
