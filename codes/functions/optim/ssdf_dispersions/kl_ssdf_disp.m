function disp = kl_ssdf_disp(m, mdl)

    disp = mdl.Tinv * m.' * log(m);
end
