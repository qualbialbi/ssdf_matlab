function disp = klnegen_ssdf_disp(m, mdl)

    disp = 0.0;
    for t = 1:mdl.T
        if m(t) <= 1
            disp = disp + kl_div( m(t), 1 );
        else
            disp = disp + -log( m(t) ) + m(t) - 1;
        end
    end

    disp = mdl.Tinv * disp;
end
