function disp = negen_ssdf_disp(m, mdl)

    disp = -mdl.Tinv * sum( log(m) );
end
