function disp = hj_ssdf_disp(m, mdl)

    disp = 0.5 * mdl.Tinv * sum(m.^2);
end
