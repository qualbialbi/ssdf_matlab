function disp = hell_disp(w, mdl)

    disp = -sum( inv_pos(-mdl.returns * w) ) * mdl.Tinv - sum(w);
end
