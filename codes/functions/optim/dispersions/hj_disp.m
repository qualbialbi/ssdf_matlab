function disp = hj_disp(w, mdl)

    disp = sum( pow_pos(mdl.returns * w, 2) ) * 0.5 * mdl.Tinv - sum(w);
end
