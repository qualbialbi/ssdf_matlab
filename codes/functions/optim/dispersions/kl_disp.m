function disp = kl_disp(w, mdl)

    disp = sum( exp(mdl.returns * w) ) * exp(-1.0) * mdl.Tinv - sum(w);
end
