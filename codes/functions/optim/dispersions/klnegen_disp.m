function disp = klnegen_disp(w, mdl)

    disp = sum( max( 
        exp(mdl.returns * w) - ones(mdl.N, 1), 
        -log( ones(mdl.N, 1) - mdl.returns * w ) 
    ) ) * mdl.Tinv - sum(w);
end
