function disp = negen_disp(w, mdl)

    disp = - 1.0 - sum( log(- mdl.returns * w) ) * mdl.Tinv - sum(w);
end
