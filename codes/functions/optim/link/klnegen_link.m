function ssdf = klnegen_link(opt_w, mdl)

    ssdf = zeros(mdl.T, 1);

    for t = 1:mdl.T
        tmp = mdl.returns(t,:) * opt_w;
        if tmp >= 1.0
            ssdf(t) = Inf;
        elseif tmp >= 0.0
            ssdf(t) = 1.0 / ( 1.0 - tmp );
        else
            ssdf(t) = exp( tmp );
        end
    end

end
