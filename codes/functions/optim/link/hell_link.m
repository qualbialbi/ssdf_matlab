function ssdf = hell_link(opt_w, mdl)
    
    ssdf = -1.0 ./ ( (mdl.returns * opt_w).^2 );
end
