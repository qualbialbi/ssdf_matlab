function pen = elnet_pen(wd, mdl)

        cvx_begin quiet
    	    variable y(mdl.Nd)
    	    minimize( sum_square_abs(y - wd) )
            subject to
                norm(y, Inf) <= mdl.tau;
    	cvx_end

     pen = 0.5 * (1.0 / mdl.alpha) * cvx_optval;
end
