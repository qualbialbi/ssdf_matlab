function pen = clinf_pen(wd, mdl)

	pen = (0.5 / mdl.alpha) * sum( huber(wd, mdl.alpha * mdl.tau) );

end
