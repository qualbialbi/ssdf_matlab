function pen = cl1_pen(wd, mdl)

        cvx_begin quiet
    	    variable z(mdl.Nd)
    	    minimize( mdl.tau * norm(z, Inf) + ...
            0.5 * (1.0 / mdl.alpha) * sum_square_abs( z - wd) )
    	cvx_end

    pen = cvx_optval;

end
