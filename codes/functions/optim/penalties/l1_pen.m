function pen = l1_pen(wd, mdl)

    pen = mdl.tau * norm(wd, Inf);
end
