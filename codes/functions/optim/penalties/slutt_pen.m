function pen = slutt_pen(wd, mdl)

	pen = (0.5 / mdl.alpha) * sum_square_pos( max(wd, 0.0) );

end
