function pen = linf_pen(wd, mdl)

    pen = mdl.tau * norm(wd, 1);
end
