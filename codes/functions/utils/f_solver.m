function res = f_solver(mdl)

    res = res_setup(mdl);
    options = optimoptions('fsolve','Algorithm','trust-region-dogleg',...
    'MaxFunEvals',1e7,'MaxIter',1e6);

    if mdl.Nd == 0
        obj_fun = mdl.div_fun;
    else
        if ~isfield(mdl, 'APT')
            obj_fun = @(w) mdl.div_fun(w, mdl) ...
                + vertcat( zeros(mdl.Ns, 1), mdl.pen_fun( w( mdl.Ns+1:end ), mdl) );
        else
            obj_fun = @(w) mdl.div_fun(w, mdl) ...
                + vertcat( zeros(mdl.Ns, 1), mdl.pen_fun( mdl.rsSig * w( mdl.Ns+1:end ), mdl) );
        end
    end

    res.opt_w(:,l) = fsolve(@(w) obj_fun(w, mdl), mdl.w_init, options);
    res.ssdf(:,l) = mdl.link_fun(res.opt_w(:,l), mdl);
    res.ssdf_div(l) = mdl.ssdf_div(res.ssdf(:,l), mdl);
    res.pricerrs(:,l) = ones(mdl.N, 1) ...
        - mdl.returns.' * res.ssdf(:,l) * mdl.Tinv;


end
