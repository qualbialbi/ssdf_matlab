function mdl = ssdf_setup(mdl)

    % set default values
    mdl = default_setup(mdl);
    
    %% set model objective function and link
    mdl = model_setup(mdl);

    %% print model
    %print_setup(mdl);
    
% DESCRIPTION
% set up SmartSDF model info in struct named mdl
% INPUT
% - mdl : (struct) containing model info
%         - fpath   : (char vector) path to .mat file containing a 1XT
%                     datetime vector saved as `dates` and a TxN matrix of
%                     returns saved as `returns` on working directory
%         - Ns      : (double) [optional] number of sure assets
%                     [default = 0]
%         - div     : (char) [optional] choice of divergence function in 
%                     the primal
%                     - 'h' : Hansen-Jagannathan divergence [default]
%                     - 'k' : Kullback-Leiber divergence
%                     - 'j' : Hansen-Jagannathan divergence also negative
%                     - 'l' : Hellinger divergence
%                     - 'n' : Negative Entropy divergence
%         - pen     : (char) [optional] choice of penalty function in the 
%                     primal [default = false]
%                     - 'r' : ridge
%                     - '2' : l2
%                     - '1' : lasso
%                     - 'i' : l-infinity
%                     - 'e' : elastic-net
%         - tau : (double) [optional] value of main penalty 
%                     parameter [default = 1]
%         - alpha   : (double) [optional] values of secondary penalty 
%                     parameter [default = 0.9]
%         - luttmer : (logical) true if weights must be nonnegative
%                     [default = false]
% OUTPUT 
% - mdl : (struct) containig following additional model info
%         - dates    : (Tx1 double vector) returns time periods
%         - returns  : (TxN double matrix) of returns
%         - N        : (double) total number of assets
%         - Nd       : (double) number dubious returns
%         - T        : (double) number of time periods
%         - Tinv     : (double) inverse of T
%         - luttmer  : (logical) true if weights must be nonnegative
%         - div_fun  : (function handle) computing dual divergence function
%         - pen_fun  : (function handle) computing dual penalty function
%         - link_fun : (function handle) computing the link between ssdf
%                      and optimal weights
%         - ssdf_div : (function handle) computing ssdf divergence
%         - pricerr_fun : (function handle) computing pricing errors metric
%         - solver   : (char) optimization solver
%                      - 'f' : fsolve if model is smooth
%                      - 'c' : cvx if model is nonsmooth

end
