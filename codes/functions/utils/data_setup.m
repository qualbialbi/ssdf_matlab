function mdl = data_setup(mdl)

    % load data
    try
        load(mdl.fpath);
    catch
        error('provide valid fpath - path to .mat file containing data');
    end
    
    if isfield(mdl,'init_date') && isfield(mdl,'end_date')
        date_idx = dates >= mdl.init_date & dates <= mdl.end_date;
    else
        date_idx = ones(numel(dates));
    end

    mdl.dates = dates(date_idx);
    mdl.T = numel(mdl.dates);
    mdl.Tinv = 1.0 / mdl.T; % set inverse of T
    mdl.returns = returns( date_idx, [1, 4+1:end] );
    mdl.N = size(mdl.returns, 2);
    mdl.Nd = mdl.N - mdl.Ns;
    mdl.rf = mdl.returns(:,1);
    mdl.FF3 = mdl.returns(:,2:4);

    % if mdl.APT is true, prepare APT dataset
    if mdl.PCA
        mdl = pca_setup(mdl);
    end

    % if mdl.APT is true, prepare APT dataset
    if mdl.APT
        mdl = apt_setup(mdl);
    end

end
