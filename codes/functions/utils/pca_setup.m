function mdl = pca_setup(mdl)
    
    fac = mdl.returns(:,2:mdl.Ns);%;(:, mdl.Ns+1:mdl.Ns+26);%
	fac=fac-mdl.returns(:,1);
	
	[G, A] = eig(cov(fac));	
	[d,ind] = sort(diag(A),'descend');
	F = (fac)*G(:,ind);
	
	F=F+mdl.returns(:,1);
	
    RS = [mdl.returns(:,[1]),F];
    mdl.returns = [RS, mdl.returns(:, mdl.Ns+1:end)];
    mdl.Ns=size(RS,2);

end