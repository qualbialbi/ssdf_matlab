function res = ssdf_run(mdl)
% DESCRIPTION
% run ssdf model mdl with penalty parameter lambda and return results in
% res (struct)
% INPUT
% - mdl : (struct) containing following relevant model info
%         - solver   : (char) optimization solver
%                      - 'f' : fsolve if model is smooth
%                      - 'c' : cvx if model is nonsmooth
% OUTPUT
% - res : (struct) containing results info
%         - ssdf_div  : ([1 x lambda_len] double vector) optimal ssdf 
%                       disperion
%         - ssdf      : ([T x lambda_len] double matrix) smartSDF
%         - pricerrs  : ([N x lambda_len] double matrix) expected pricing 
%                       errors under smartSDF
%         - opt_w     : ([N x lambda_len] double vector) optimal portfolio 
%                       weights
%         - mdl       : (struct) containing model info
   
    if mdl.solver == 'f'     
        res = f_solver(mdl);
    else
        res = c_solver(mdl);
    end
            
    %res.mdl = mdl;
end
