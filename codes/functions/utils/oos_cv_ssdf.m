function oos = oos_cv_ssdf(mdl, wnd_train, wnd_test, dates, returns, rf)

    oop = floor( (length(dates) - wnd_train) / wnd_test );
    oos.oos_ssdf = zeros( oop * wnd_test , 1 );

    oos.opt_taus = zeros(oop,1);
    oos.in_opt_we = zeros( size(returns, 2), oop );
    
    for i = 1:oop

        tmp = mdl;
        bot = (i - 1) * wnd_test + 1;
        top = (i - 1) * wnd_test + wnd_train;

        tmp.date_init = dates(bot);
        tmp.date_end = dates(top);
        tmp.taus = linspace(0, tmp.taumax(i), 100);

        tmp = ssdf_setup(tmp);
        res = ssdf_run(tmp);
        GLSRsqAdj = -ones(100, 1);
        
            
        
        for t = 1:100
            if tmp.pricerr_fun(res.pricerrs(:,t)) > tmp.taus(t)+1e-4
                 continue
            end
            % first step
            exret = tmp.returns(:, tmp.Ns+1:end) - tmp.rf;
            X = [ ones( length(res.ssdf(:,t)) , 1), res.ssdf(:,t) ];
            alpha_beta = ( (X'* X) \ (X'* oos_exret) )';
            beta = alpha_beta(:, 2);
   
            % second step
            av_exret = mean(exret, 1)';
            if tmp.shrink
                V = shrinkage_cov(exret);
            else
                V = cov(exret);
            end

            [Q, D] = eig(V);
            Vsqrt = Q * sqrt(abs(D)) * Q';
            fm_gls = fitlm( Vsqrt \ [ones(length(beta), 1), beta], ...
                Vsqrt \ av_exret, 'Intercept', false);
            GLSRsqAdj(t)=fm_gls.Rsquared.Adjusted;
        end
        [a,b]=max(GLSRsqAdj);
        oos.opt_taus(i) = tmp.taus(b);
        oos.in_opt_we(:,i)=res.opt_w(:,b);
        tmp.returns = returns(top+1:top+wnd_test, : );
        oos.oos_ssdf(bot:i*wnd_test)= tmp.link_fun(res.opt_w(:,b), tmp);  
    end
    
    oos_idx = tmp.dates >= tmp.dates(wnd_train+1);
    % first step
    oos_exret = returns(oos_idx, tmp.Ns+1:end) - rf(oos_idx);
    X = [ones(length(oos.oos_ssdf), 1), oos.oos_ssdf];
    alpha_beta = ( (X'* X) \ (X'* oos_exret) )';
    beta = alpha_beta(:, 2);
  
    % second step
    av_exret = mean(oos_exret, 1)';
    oos.fm = fitlm(beta, av_exret);
  
    if tmp.shrink
        V = shrinkage_cov(oos_exret);
    else
        V = cov(oos_exret);
    end
  
   [Q, D] = eig(V);
   Vsqrt = Q * sqrt(abs(D)) * Q';
   oos.fm_gls = fitlm( Vsqrt \ [ones(length(beta), 1), beta], ...
       Vsqrt \ av_exret, 'Intercept', false);

end
