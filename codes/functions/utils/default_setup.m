function mdl = default_setup(mdl)

    %% set default values and check input, load data
    % throw error if fpath is not provided
    if ~isfield(mdl, 'fpath')
        error('Provide fpath - path to .mat file containing data');
    elseif ~ischar(mdl.fpath)
        error('mdl.fpath must be of type char');
    end

    % default all-dubious assets
    if ~isfield(mdl, 'Ns')
        mdl.Ns = 0; 
    elseif ~isnumeric(mdl.Ns)
        error('mdl.Ns must be numeric');
    end

    % set data
    mdl = data_setup(mdl);

    % set default primal divergence 'h'
    divs = {'hj','kl','jh','ne','he'};
    if ~isfield(mdl, 'div')
        mdl.div = 'he';
    elseif ~any(strcmp(mdl.div,divs))
        error('mdl.div must be a valid divergence'); 
    end

    % set default primal penalty
    pens = {'rd','l2','l1','li','el','cli','sl','lu','np'};
    if mdl.Nd == 0;
        mdl.pen = 'np';
    end
    if ~isfield(mdl, 'pen')
        mdl.pen = 'np';
    end
    if ~any(strcmp(mdl.pen,pens))
        error('mdl.pen must a valid penalty');
    end

    % set default lambda values
    if any(strcmp(mdl.pen,{'rd','l2','l1','li','el','cli','sl'}))
	if ~isfield(mdl, 'taus')
	    error('mdl.taus must be provided');
	end
	if ~isnumeric(mdl.taus)
	    error('mdl.taus must be numeric'); 
	end
    else
        mdl.taus=1.0;
    end
    mdl.taus_len = length(mdl.taus);

    % set default alpha values if pen = 'e'
    if any(strcmp(mdl.pen,{'el','cli','sl'}))
        if ~isfield(mdl, 'alpha')
            error('mdl.alpha must be provided');
        end
        if ~isnumeric(mdl.alpha)
            error('mdl.alpha must be numeric');
        end
    end

    % set model solver depending on differentiability
    if any(strcmp(mdl.pen,{'l2','l1','li','cli','sl','lu','np'}))
        mdl.solver = 'c';
    else
        mdl.solver = 'f';
    end    
    
    % set default w_init
    mdl.w_init = ones(mdl.N, 1);
    

end
