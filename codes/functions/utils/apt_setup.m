function mdl= apt_setup(mdl)

   if mdl.Ns==1 && ~isfield(mdl,'av')
        Rd = mdl.returns(:, 2:end)-mdl.rf;
        if mdl.shrink==true
            Sig=shrinkage_cov(Rd);
        else
            Sig=cov(Rd);
        end
            [Q, D] = eig(Sig);
            mdl.rsSig = Q * sqrt(D) * Q';
   else
       
        F = mdl.returns(:, 1+1:1+mdl.N_apt)-mdl.rf;
        Rd = mdl.returns(:, 1+mdl.N_apt+1:end)-mdl.rf;	

        beta=zeros(size(F,2),size(Rd,2));

        for i=1:size(Rd,2)
            beta(:,i)=((F'*F)\F'*Rd(:,i));
        end

        if mdl.shrink==true
            Sig=shrinkage_cov(Rd-F*beta);
        else
            Sig=cov(Rd-F*beta);
        end
        [Q, D] = eig(Sig);
        mdl.rsSig = Q * sqrt(D) * Q';

   end
end
