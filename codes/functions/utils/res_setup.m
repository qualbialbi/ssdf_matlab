function res = res_setup(mdl)
% DESCRIPTION
% setup res (struct) holder for result info
% INPUT
% - mdl : (struct) containig model info
%         - dates   : (Tx1 double vector) returns time periods
%         - returns : (TxN double matrix) of returns
%         - N       : (double) total number of assets
%         - Ns      : (double) number of sure assets
%         - Nd      : (double) number dubious returns
%         - T       : (double) number of time periods
%         - Tinv    : (double) inverse of T
%         - div     : (char) choice of divergence function in the primal
%                     - 'h' : Hansen-Jagannathan divergence
%                     - 'k' : Kullback-Leiber divergence
%         - pen     : (char) choice of penalty function in the primal
%                     - 'r' : ridge
%                     - 'l' : lasso
%                     - 'i' : l-infinity
%                     - 'e' : conjugate of elastic-net
%                     - 'n' : no penalty
%         - obj_fun  : (function handle) computing dual objective function 
%         - link_fun : (function handle) computing the link between ssdf
%                      and optimal weights
%         - ssdf_div : (function handle) computing ssdf divergence
%         - lambdas  : (double vector) containing values of main penalty 
%                      parameter
%         - alpha    : (double) value of optional penalty parameter
% OUTPUT
% - res : (struct) holder for results info
%         - ssdf_div  : (1xlambda_len double vector) optimal ssdf disperion
%         - ssdf      : (Txlambda_len double vector) smartSDF
%         - pricerrs  : (Nxlambda_len double vector) expected pricing 
%                       errors under smartSDF
%         - opt_w     : (Nxlambda_len double vector) optimal portfolio 
%                       weights
    
    res.ssdf_div   = zeros(1, mdl.taus_len);
    res.ssdf       = zeros(mdl.T, mdl.taus_len);
    res.pricerrs   = zeros(mdl.N, mdl.taus_len);
    res.opt_w      = zeros(mdl.N, mdl.taus_len);
end
