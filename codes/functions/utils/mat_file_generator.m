function mat_file_generator(filepath, matpath, year, month, day)
% DESCRIPTION
% generate mat file from .txt file in filepath and store .mat file in 
% matpath as `data`
% INPUT
% filepath   : (char vector) path to .txt file containing one column of 
%              dates and in the next columns data on returns
% matpath    : (char vector) path to .mat file where to store data

    data = readtable(filepath);
    returns = data{:,2:end};
    dates = datetime(year, month, day, 'Format', 'yyyy MM') ...
        + calmonths(0:size(data, 1)-1);
    save(matpath, 'dates', 'returns');
  
end
