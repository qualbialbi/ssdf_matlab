function print_setup(mdl)
% function printing the model to the console

    %% MODEL
    fprintf('\n\nMODEL\n=====');
    switch mdl.div
        case 'h'
            divergence = 'Hansen-Jagannathan';
        case 'k'
            divergence = 'Kullback-Leiber';
        case 'j'
            divergence = 'Hansen-Jagannathan also negative';
        case 'n'
            divergence = 'Negative Entropy';
        otherwise
            divergence = 'Hellingher'
    end
    fprintf('\nprimal divergence: %s', divergence);
    switch mdl.pen
        case 'r'
            penalty = 'Ridge';
        case '2'
            penalty = 'l-2'
        case 'l'
            penalty = 'Lasso';
        case 'i'
            penalty = 'l-infinity';
        case 'e'
            penalty = 'Elastic Net';
        otherwise
            penalty = 'No Penalization';
    end
    fprintf('\nprimal penalty: %s', penalty);
    fprintf('\nAPT: %u', mdl.APT);
    if mdl.oos
        fprintf('\nOOS: %u', mdl.oos);
        fprintf('\nOOS window: %u', mdl.wnd);
    end

    %% PARAMETERS
    fprintf('\n\nPARAMETERS\n=====');
    fprintf('\nnumber of assets: %u', mdl.N);
    fprintf('\nnumber of sure assets: %u', mdl.Ns);
    fprintf('\nnumber of dubious assets: %u', mdl.Nd);
    fprintf('\nnumber of time periods: %u', mdl.T);
    
    if mdl.pen ~= false
        if numel(mdl.lambdas) > 1
            fprintf('\nnumber of values of lambda: %u', mdl.lambda_len);
            fprintf('\nfirst and last values of lambda: %f -> %f', ...
                mdl.lambdas(1), mdl.lambdas(end));
        else
            fprintf('\nvalue of lambda: %u', mdl.lambdas);
        end
    end
    
    fprintf('\n\nSOLVER\n======');
    switch mdl.solver
        case 'f'
            solver = 'fsolve';
        otherwise
            solver = 'cvx';
    end
    fprintf('\n%s\n\n', solver);
end
