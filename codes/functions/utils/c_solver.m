function res = c_solver(mdl)

    res = res_setup(mdl);

    if mdl.Nd == 0 | any(strcmp(mdl.pen,{'lu','np'}))
        obj_fun = mdl.div_fun;
    else
        if ~mdl.APT
            obj_fun = @(w, mdl) mdl.div_fun(w, mdl) ...
                + mdl.pen_fun( w( mdl.Ns+1:end ), mdl );
        else 
            obj_fun = @(w, mdl) mdl.div_fun(w, mdl) ...
                + mdl.pen_fun( mdl.rsSig * w( mdl.Ns+1:end ), mdl );
        end
    end
    
    if strcmp(mdl.pen,{'lu'})
        cvx_begin quiet
    	    variable w(mdl.N)
    	    minimize( obj_fun(w, mdl) )
            subject to
                w(mdl.Ns+1:end)<=0
    	cvx_end
            
            if strcmp(mdl.div,{'ne'})
    	        w = -w;
            end
            res.opt_w = w;
    	    res.ssdf = mdl.link_fun(res.opt_w, mdl);
    	    res.ssdf_div = mdl.ssdf_div(res.ssdf, mdl);
    	    res.pricerrs(:,l) = ones(mdl.N, 1) ...
        	- mdl.returns.' * res.ssdf * mdl.Tinv;
    else
    	for l=1:mdl.taus_len
            mdl.tau = mdl.taus(l);
    	    cvx_begin quiet
    	        variable w(mdl.N)
    	        minimize( obj_fun(w, mdl) )
    	    cvx_end
            
            if strcmp(mdl.div,{'ne'})
    	        w = -w;
            end
    	    res.opt_w(:,l) = w;
    	    res.ssdf(:,l) = mdl.link_fun(res.opt_w(:,l), mdl);
    	    res.ssdf_div(l) = mdl.ssdf_div(res.ssdf(:,l), mdl);
    	    res.pricerrs(:,l) = ones(mdl.N, 1) ...
        	- mdl.returns.' * res.ssdf(:,l) * mdl.Tinv;
        end
    end

    if any(strcmp(mdl.pen,{'lu','sl'}))
        res.opt_w(mdl.Ns+1:end,:) = -res.opt_w(mdl.Ns+1:end,:);
    end
end
