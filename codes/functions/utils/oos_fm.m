function oos = oos_fm(mdl, wnd_train, wnd_test, oos_eval_dates)

    tmp = mdl;
    tmp.taus = 1.0;
    tmp.init_date = oos_eval_dates(1);
    tmp.end_date = oos_eval_dates(2);

    tmp = ssdf_setup(tmp);
    oos = oos_sdf(tmp, wnd_train, wnd_test, tmp.dates, tmp.returns);

    oos_idx = tmp.dates >= tmp.dates(wnd_train+1);
    oos.oos_pe = ones(tmp.N, 1) - ...
        ( tmp.returns(oos_idx, :).'* oos.oos_ssdf ) / sum(oos_idx);

    % first step
    oos_exret = tmp.returns(oos_idx, tmp.Ns+1:end) - tmp.rf(oos_idx);
    X = [ones(length(oos.oos_ssdf), 1), oos.oos_ssdf];
    alpha_beta = ( (X'* X) \ (X'* oos_exret) )';
    beta = alpha_beta(:, 2);

    % second step
    av_exret = mean(oos_exret, 1)';
    oos.fm = fitlm(beta, av_exret);

    if tmp.shrink==true
        V = shrinkage_cov(oos_exret);
    else
	    V = cov(oos_exret);
    end

    [Q, D] = eig(V);
    Vsqrt = Q * sqrt(abs(D)) * Q';
    oos.fm_gls = fitlm( Vsqrt \ [ones(length(beta), 1), beta], ...
        Vsqrt \ av_exret, 'Intercept', false);

end
