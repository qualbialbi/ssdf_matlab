function mdl = model_setup(mdl)

    % divergence function
    switch mdl.div
        case 'hj'
            if mdl.solver == 'c'
                mdl.div_fun = @hj_obj;
            else
                mdl.div_fun = @hj_grad;
            end
            mdl.link_fun = @hj_link;
            mdl.ssdf_div = @hj_ssdf_div;
        case 'kl'
            if mdl.solver == 'c'
                mdl.div_fun = @kl_obj;
            else
                mdl.div_fun = @kl_grad;
            end
            mdl.link_fun = @kl_link;
            mdl.ssdf_div = @kl_ssdf_div;
        case 'jh'
            if mdl.solver == 'c'
                mdl.div_fun = @hjn_obj;
            else
                mdl.div_fun = @hjn_grad;
            end
            mdl.link_fun = @hjn_link;
            mdl.ssdf_div = @hjn_ssdf_div;
        case 'ne'
            if mdl.solver == 'c'
                mdl.div_fun = @negen_obj;
            else
                mdl.div_fun = @negen_grad;
            end
            mdl.link_fun = @negen_link;
            mdl.ssdf_div = @negen_ssdf_div;
        case 'he'
            if mdl.solver == 'c'
                mdl.div_fun = @hell_obj;
            else
                mdl.div_fun = @hell_grad;
            end
            mdl.link_fun = @hell_link;
            mdl.ssdf_div = @hell_ssdf_div;
    end

    switch mdl.pen
        case 'rd'
            mdl.pen_fun = @ridge_grad;
        case 'l2'
            mdl.pen_fun = @l2_obj;
            mdl.pricerr_fun = @pricerr_l2;
        case 'l1'
            mdl.pen_fun = @lasso_obj;
            mdl.pricerr_fun = @pricerr_l1;
        case 'li'
            mdl.pen_fun = @linf_obj;
            mdl.pricerr_fun = @pricerr_linf;
        case 'el'
            mdl.pen_fun = @elnet_grad;
        case 'cli'
            mdl.pen_fun = @clinf_obj;
        case 'sl'
            mdl.pen_fun = @slutt_obj;
    end

% DESCRIPTION
% set up information of the portfolio problem objective function and
% the link between optimal portfolio weights and ssdf
% INPUT
% - mdl : (struct) containig following relevant model info
%         - div      : (char) choice of divergence function in the primal
%                      - 'h' : Hansen-Jagannathan divergence
%                      - 'k' : Kullback-Leiber divergence
%                      - 'j' : Hansen-Jagannathan divergence also negative
%                      - 'n' : Negative Entropy divergence
%                      - 'l' : Hellingher dispersion
%         - pen      : (char) choice of penalty function in the primal
%                      - 'r' : ridge
%                      - '2' : l2
%                      - 'l' : lasso
%                      - 'i' : l-infinity
%                      - 'e' : elastic-net
% OUTPUT
% - mdl.div_fun  : (function handle) computing dual divergence function
% - mdl.pen_fun  : (function handle) computing dual penalty function
% - mdl.link_fun : (function handle) computing the link between ssdf and 
%              optimal weights
% - mdl.ssdf_div : (function handle) computing ssdf divergence
% - pricerr_fun : (function handle) computing pricing errors metric
% - solver   : (char) optimization solver
%              - 'f' : fsolve if model is smooth
%              - 'c' : cvx if model is nonsmooth
    

end
