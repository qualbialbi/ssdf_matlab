function oos = oos_sdf(mdl, wnd_train, wnd_test, dates, returns)

    oop = floor( (length(dates) - wnd_train) / wnd_test );
    oos.oos_ssdf = zeros( oop * wnd_test , 1 );
    
    oos.in_pric_er = zeros( size(returns, 2), oop );
    oos.in_pric_er_h = zeros(oop, 1);
    oos.in_opt_we = zeros( size(returns, 2), oop );

    for i = 1:oop

        tmp = mdl;
        bot = (i - 1) * wnd_test + 1;
        top = (i - 1) * wnd_test + wnd_train;

	    tmp.date_init = dates(bot);
	    tmp.date_end = dates(top);
	    tmp.taus = tmp.taumax(i) * tmp.tau_rat;

	    tmp = ssdf_setup(tmp); 
        res = ssdf_run(tmp);
        
        oos.in_pric_er(:, i) = res.pricerrs;
        oos.in_pric_er_h(i) = tmp.pricerr_fun( ...
            res.pricerrs( tmp.Ns+1:end ), tmp );
        oos.in_opt_we(:, i) = res.opt_w;
        tmp.returns = returns( top+1 : top+wnd_test , :);
        
        if ( norm(res.pricerrs(1:tmp.Ns), Inf) > 1e-2 || ...
            oos.in_pric_er_h(i) > tmp.taus + 1e-4 )
            oos.oos_ssdf( bot : i*wnd_test ) = NaN;
        else
            oos.oos_ssdf( bot:i*wnd_test ) = tmp.link_fun(res.opt_w, tmp);
        end
    end

end
