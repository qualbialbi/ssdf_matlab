%% S = FF3F ANALYSIS

% add paths
paths_setup();
% setup cvx
cvx_setup;

% set mdl
mdl.fpath = 'FF3F_monthly.mat';
mdl.Ns = 4;

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_FF3F.res_h = ssdf_run(mdl);

% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_FF3F.res_j = ssdf_run(mdl);

% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_FF3F.res_k = ssdf_run(mdl);

% run ssdf 'n'
mdl.div = 'n';
mdl = ssdf_setup(mdl);
res_FF3F.res_n = ssdf_run(mdl);
res=ssdf_run(mdl);

% run ssdf 'l'
mdl.div = 'l';
mdl = ssdf_setup(mdl);
res_FF3F.res_l = ssdf_run(mdl);

% compute pricing errors on FF100P
load('FF100P_monthly.mat');
T = numel(dates);
N = size(returns, 2);
res_FF3F.res_h.pricerrs_100P = ones(N, 1) - ...
    returns.' * res_FF3F.res_h.ssdf / T;
res_FF3F.res_j.pricerrs_100P = ones(N, 1) - ...
    returns.' * res_FF3F.res_j.ssdf / T;
res_FF3F.res_k.pricerrs_100P = ones(N, 1) - ...
    returns.' * res_FF3F.res_k.ssdf / T;
res_FF3F.res_n.pricerrs_100P = ones(N, 1) - ...
    returns.' * res_FF3F.res_n.ssdf / T;
res_FF3F.res_l.pricerrs_100P = ones(N, 1) - ...
    returns.' * res_FF3F.res_l.ssdf / T;

% save
save('res_FF3F.mat','res_FF3F');

%% S = FF3F100P ANALYSIS

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 104;

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_allsure_FF3F100P.res_h = ssdf_run(mdl);

% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_allsure_FF3F100P.res_j = ssdf_run(mdl);

% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_allsure_FF3F100P.res_k = ssdf_run(mdl);

% run ssdf 'n'
mdl.div = 'n';
mdl = ssdf_setup(mdl);
mdl.div_fun=@negen_obj;
mdl.solver='c';
res_allsure_FF3F100P.res_n = ssdf_run(mdl);

% run ssdf 'l'
mdl.div = 'l';
mdl = ssdf_setup(mdl);
mdl.div_fun=@hell_obj;
mdl.solver='c';
res_allsure_FF3F100P.res_l = ssdf_run(mdl);

% save
save('res_allsure_FF3F100P.mat','res_allsure_FF3F100P');


%% S = 3F; D = 100P; l2

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 4;
mdl.pen = '2';
% find intelligent lambdas
load('res_FF3F.mat');
max_tau = norm( res_FF3F.res_h.pricerrs_100P , 2 );
eps = 1e-3;
mdl.lambdas = logspace(0, 4, 50) / 1e4 * (max_tau+eps);

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_h_l2 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_h_l2.ssdf_div)

% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_j_l2 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_j_l2.ssdf_div)

% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_k_l2 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_k_l2.ssdf_div)

% run ssdf 'n'
mdl.div = 'n';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_n_l2 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_n_l2.ssdf_div)

% run ssdf 'l'
mdl.div = 'l';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_l_l2 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_l_l2.ssdf_div)


%% S = 3F; D = 100P; l1

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 4;
mdl.pen = 'l';
% find intelligent lambdas
load('res_FF3F.mat');
max_tau = norm( res_FF3F.res_h.pricerrs_100P , 1 );
eps = 1e-3;
mdl.lambdas = logspace(0, 4, 50) / 1e4 * (max_tau+eps);

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_h_l1 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_h_l1.ssdf_div)


% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_j_l1 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_j_l1.ssdf_div)


% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_k_l1 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_k_l1.ssdf_div)


% run ssdf 'n'
mdl.div = 'n';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_n_l1 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_n_l1.ssdf_div)


% run ssdf 'l'
mdl.div = 'l';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_l_l1 = ssdf_run(mdl);
plot(mdl.lambdas, res_FF3F100P.res_l_l1.ssdf_div)


%% S = 3F; D = 100P; linf

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 4;
mdl.pen = 'i';
% find intelligent lambdas
load('res_FF3F.mat');
max_tau = norm( res_FF3F.res_h.pricerrs_100P , Inf );
eps = 1e-3;
mdl.lambdas = logspace(0, 4, 50) / 1e4 * (max_tau+eps);

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_h_li = ssdf_run(mdl);

% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_j_li = ssdf_run(mdl);

% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_k_li = ssdf_run(mdl);

% run ssdf 'n'
mdl.div = 'n';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_n_li = ssdf_run(mdl);

% run ssdf 'l'
mdl.div = 'l';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_l_li = ssdf_run(mdl);

save('res_FF3F100P.mat', 'res_FF3F100P');


%% S = 3F; D = 100P; elnet

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 4;
mdl.pen = 'e';
mdl.alpha = 0.8;
% find intelligent lambdas
mdl.lambdas = logspace(0, 4, 500);

% run ssdf 'h'
mdl.div = 'h';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_h_elnet = ssdf_run(mdl);

% run ssdf 'j'
mdl.div = 'j';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_j_elnet = ssdf_run(mdl);

% run ssdf 'k'
mdl.div = 'k';
mdl = ssdf_setup(mdl);
res_FF3F100P.res_k_elnet = ssdf_run(mdl);

save('res_FF3F100P.mat', 'res_FF3F100P');

%% load
load('res_FF3F');

%% PLOTS AND DISPLAYS

% HJ
dates = res_FF3F100P.res_h_l2.mdl.dates;
max_tau_l2 = norm( res_FF3F.res_h.pricerrs_100P , 2 );
max_tau_l1 = norm( res_FF3F.res_h.pricerrs_100P , 1 );
max_tau_li = norm( res_FF3F.res_h.pricerrs_100P , Inf );
eps = 1e-3;
taus_l2 = logspace(0, 4, 50) / 1e4 * (max_tau_l2+eps);
taus_l1 = logspace(0, 4, 50) / 1e4 * (max_tau_l1+eps);
taus_li = logspace(0, 4, 50) / 1e4 * (max_tau_li+eps);

% plot ssdf hj div
plot(taus_l2, res_FF3F100P.res_h_l2.ssdf_div,...
    taus_l1, res_FF3F100P.res_h_l1.ssdf_div, ...
    taus_li, res_FF3F100P.res_h_li.ssdf_div)

% plot pricerrs
% plot pricerrs l2
bar(res_FF3F.res_h.pricerrs_100P);
bar(res_FF3F100P.res_h_l2.pricerrs(5:end,40));
bar(res_FF3F100P.res_h_l2.pricerrs(5:end,30));
bar(res_FF3F100P.res_h_l2.pricerrs(5:end,20));
bar(res_FF3F100P.res_h_l2.pricerrs(5:end,10));
% plot pricerrs l1
bar(res_FF3F.res_h.pricerrs_100P);
bar(res_FF3F100P.res_h_l1.pricerrs(5:end,40));
bar(res_FF3F100P.res_h_l1.pricerrs(5:end,30));
bar(res_FF3F100P.res_h_l1.pricerrs(5:end,20));
bar(res_FF3F100P.res_h_l1.pricerrs(5:end,10));
% plot pricerrs li
bar(res_FF3F.res_h.pricerrs_100P);
bar(res_FF3F100P.res_h_li.pricerrs(5:end,40));
bar(res_FF3F100P.res_h_li.pricerrs(5:end,30));
bar(res_FF3F100P.res_h_li.pricerrs(5:end,20));
bar(res_FF3F100P.res_h_li.pricerrs(5:end,10));

% plot opt weights
% l2
bar(res_FF3F.res_h.opt_w);
bar(res_FF3F100P.res_h_l2.opt_w(:,40));
bar(res_FF3F100P.res_h_l2.opt_w(:,30));
bar(res_FF3F100P.res_h_l2.opt_w(:,20));
bar(res_FF3F100P.res_h_l2.opt_w(:,10));
% l1
bar(res_FF3F.res_h.opt_w);
bar(res_FF3F100P.res_h_l1.opt_w(:,40));
bar(res_FF3F100P.res_h_l1.opt_w(:,30));
bar(res_FF3F100P.res_h_l1.opt_w(:,20));
bar(res_FF3F100P.res_h_l1.opt_w(:,10));
% li
bar(res_FF3F.res_h.opt_w);
bar(res_FF3F100P.res_h_li.opt_w(:,40));
bar(res_FF3F100P.res_h_li.opt_w(:,30));
bar(res_FF3F100P.res_h_li.opt_w(:,20));
bar(res_FF3F100P.res_h_li.opt_w(:,10));


% KL
dates = res_FF3F100P.res_k_l2.mdl.dates;
max_tau_l2 = norm( res_FF3F.res_k.pricerrs_100P , 2 );
max_tau_l1 = norm( res_FF3F.res_k.pricerrs_100P , 1 );
max_tau_li = norm( res_FF3F.res_k.pricerrs_100P , Inf );
eps = 1e-3;
taus_l2 = logspace(0, 4, 50) / 1e4 * (max_tau_l2+eps);
taus_l1 = logspace(0, 4, 50) / 1e4 * (max_tau_l1+eps);
taus_li = logspace(0, 4, 50) / 1e4 * (max_tau_li+eps);

% plot ssdf hj div
plot(taus_l2, res_FF3F100P.res_k_l2.ssdf_div,...
    taus_l1, res_FF3F100P.res_k_l1.ssdf_div, ...
    taus_li, res_FF3F100P.res_k_li.ssdf_div)

% plot pricerrs
% plot pricerrs l2
bar(res_FF3F.res_k.pricerrs_100P);
bar(res_FF3F100P.res_k_l2.pricerrs(5:end,40));
bar(res_FF3F100P.res_k_l2.pricerrs(5:end,30));
bar(res_FF3F100P.res_k_l2.pricerrs(5:end,20));
bar(res_FF3F100P.res_k_l2.pricerrs(5:end,10));
% plot pricerrs l1
bar(res_FF3F.res_k.pricerrs_100P);
bar(res_FF3F100P.res_k_l1.pricerrs(5:end,40));
bar(res_FF3F100P.res_k_l1.pricerrs(5:end,30));
bar(res_FF3F100P.res_k_l1.pricerrs(5:end,20));
bar(res_FF3F100P.res_k_l1.pricerrs(5:end,10));
% plot pricerrs li
bar(res_FF3F.res_k.pricerrs_100P);
bar(res_FF3F100P.res_k_li.pricerrs(5:end,40));
bar(res_FF3F100P.res_k_li.pricerrs(5:end,30));
bar(res_FF3F100P.res_k_li.pricerrs(5:end,20));
bar(res_FF3F100P.res_k_li.pricerrs(5:end,10));

% plot opt weights
% l2
bar(res_FF3F.res_k.opt_w);
bar(res_FF3F100P.res_k_l2.opt_w(:,40));
bar(res_FF3F100P.res_k_l2.opt_w(:,30));
bar(res_FF3F100P.res_k_l2.opt_w(:,20));
bar(res_FF3F100P.res_k_l2.opt_w(:,10));
% l1
bar(res_FF3F.res_k.opt_w);
bar(res_FF3F100P.res_k_l1.opt_w(:,40));
bar(res_FF3F100P.res_k_l1.opt_w(:,30));
bar(res_FF3F100P.res_k_l1.opt_w(:,20));
bar(res_FF3F100P.res_k_l1.opt_w(:,10));
% li
bar(res_FF3F.res_k.opt_w);
bar(res_FF3F100P.res_k_li.opt_w(:,40));
bar(res_FF3F100P.res_k_li.opt_w(:,30));
bar(res_FF3F100P.res_k_li.opt_w(:,20));
bar(res_FF3F100P.res_k_li.opt_w(:,10));


% plot ssdf S=3F
plot(dates, res_FF3F.res_h.ssdf);
plot(dates, res_FF3F.res_j.ssdf);
plot(dates, res_FF3F.res_k.ssdf);
plot(dates, res_FF3F.res_n.ssdf);
plot(dates, res_FF3F.res_l.ssdf);

% plot ssdf S=3F100P
plot(dates, res_allsure_FF3F100P.res_h.ssdf);
plot(dates, res_allsure_FF3F100P.res_j.ssdf);
plot(dates, res_allsure_FF3F100P.res_k.ssdf);
plot(dates, res_allsure_FF3F100P.res_n.ssdf);
plot(dates, res_allsure_FF3F100P.res_l.ssdf);

% plot ssdf S=3F D=100P
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,1));
plot(dates, res_allsure_FF3F100P.res_h.ssdf);
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,10));
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,20));
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,30));
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,40));
plot(dates, res_FF3F100P.res_h_l2.ssdf(:,50));
plot(dates, res_FF3F.res_h.ssdf);



% display ssdf dispersion
dats = [res_FF3F_h.ssdf_div, res_FF3F_k.ssdf_div, res_FF3F_j.ssdf_div];
bar(dats);

% display opt_weights
bar(res_FF3F_h.opt_w);
bar(res_FF3F_k.opt_w);
bar(res_FF3F_j.opt_w);

% display pricerrs on FF3F
bar(res_FF3F_h.pricerrs);
bar(res_FF3F_k.pricerrs);
bar(res_FF3F_j.pricerrs);

% compute pricerrs on FF25P
load('FF25P_monthly.mat');
T = size(returns, 1);
N = size(returns, 2);
pricerrs_25P_h = ones(N, 1) - returns.' * res_FF3F_h.ssdf / T;
pricerrs_25P_k = ones(N, 1) - returns.' * res_FF3F_k.ssdf / T;
pricerrs_25P_j = ones(N, 1) - returns.' * res_FF3F_j.ssdf / T;
barh(pricerrs_25P_h);
barh(pricerrs_25P_k);
barh(pricerrs_25P_j);

