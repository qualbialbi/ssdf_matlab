clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
%cvx_setup;%load cvx
mdl.fpath = 'RF_FF3_Inter.mat';
mdl.Ns = 1;
mdl.div = 'jh';
mdl.pen = 'li';
mdl.APT=false;
mdl.PCA=false;

mdl.alpha=1e-3;
%mdl.lambda=0.05;

oos_eval_dates=[datetime(1963,07,01),datetime(2010,12,30)];
%%

mdl_1=mdl;
nor=taumax_func(mdl_1);
mdl_1.taus=nor/5;

wnd_train=360;
wnd_test=12;

mdl_1 = ssdf_setup(mdl_1);

[oos_ssdf,oos_dates,lmd]=oos_fm(mdl_1,wnd_train,wnd_test,oos_eval_dates);
lmd
%%
plot_sdf_oos(oos_ssdf,oos_dates)
