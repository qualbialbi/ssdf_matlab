%% test ssdf setup

paths_setup();
% load cvx
cvx_setup;

%% SETUP
% path to sure and dubious assets
% - fpath   : (char vector) path to .mat file containing a 1XT
%             datetime vector saved as `dates` and a TxN matrix of
%             returns saved as `returns` on working directory
mdl.fpath = 'FF3F100P_monthly.mat';

% number of sure assets
% - mdl.Ns      : (double) [optional] number of sure assets
%                 [default = 0]
mdl.Ns = 4;

% model primal divergence
% - mdl.div     : (char) [optional] choice of divergence function in 
%                 the primal
%                 - 'h' : Hansen-Jagannathan divergence [default]
%                 - 'k' : Kullback-Leiber divergence
%                 - 'j' : Hansen-Jagannathan divergence also negative
mdl.div = 'h';

% model primal penalty
% - mdl.pen     : (char) [optional] choice of penalty function in the 
%                 primal [default = false]
%                 - 'r' : ridge
%                 - '2' : l2
%                 - 'l' : lasso
%                 - 'i' : l-infinity
%                 - 'e' : elastic-net
mdl.pen = 'r';

% model lambda values
% - mdl.lambdas : (double vector) [optional ]values of main penalty 
%                  parameter [default = 1]
mdl.lambdas = 0.1;% logspace(0, 3, 100) / 1e2; % logspace(0, 3, 100) / 1e3;
%mdl.alpha = 0.1;

% mdl.APT true
%mdl.APT = true;
mdl.solver = 'c';
% setup mdl
mdl = ssdf_setup(mdl);
% OUTPUT
% - mdl : (struct) containing model info
%         - fpath   : (char vector) path to .mat file containing a matrix
%                     with dates (first column) and returns (other columns)
%                     saved as `data` on working directory
%         - Ns      : (double) [optional] number of sure assets
%                     [default = 0]
%         - div     : (char) [optional] choice of divergence function in 
%                     the primal
%                     - 'h' : Hansen-Jagannathan divergence [default]
%                     - 'k' : Kullback-Leiber divergence
%                     - 'j' : Hansen-Jagannathan divergence also negative
%         - pen     : (char) [optional] choice of penalty function in the 
%                     primal [default = false]
%                     - 'r' : ridge
%                     - 'l' : lasso
%                     - 'i' : l-infinity
%                     - 'e' : elastic-net
%         - lambdas : (double vector) [optional] values of main penalty 
%                     parameter [default = 1]
%         - alpha   : (double) [optional] elasticnet penalty parameter 
%                     [default = 0.9]
%         - w_init  : (double vector) [optional] initial portfolio weights 
%                     [default = ones(mdl.N, 1)]
%        - dates    : (Tx1 double vector) returns time periods
%        - returns  : (TxN double matrix) of returns
%        - N        : (double) total number of assets
%        - Nd       : (double) number dubious returns
%        - T        : (double) number of time periods
%        - Tinv     : (double) inverse of T
%        - lambda_len : (double) size of mdl.lambdas
%        - obj_fun  : (function handle) computing dual objective function 
%        - link_fun : (function handle) computing the link between ssdf
%                     and optimal weights
%        - ssdf_div : (function handle) computing ssdf divergence
%        - solver   : (char) optimization solver
%                     - 'f' : fsolve if model is smooth
%                     - 'c' : cvx if model is nonsmooth

%% RUN

res1 = ssdf_run(mdl);
% OUTPUT
% - res : (struct) containing results info
%         - ssdf_div  : ([1 x lambda_len] double vector) optimal ssdf 
%                       disperion
%         - ssdf      : ([T x lambda_len] double matrix) smartSDF
%         - pricerrs  : ([N x lambda_len] double matrix) expected pricing 
%                       errors under smartSDF
%         - opt_w     : ([N x lambda_len] double vector) optimal portfolio 
%                       weights
%         - lambdas   : (double vector) [optional] values of main penalty 
%                       parameter [default = 1]
10000*mean(abs(res.pricerrs))