function [oos_ssdf,oos_dates,lmd]=oos_fm(mdl_1,wnd_train,wnd_test,oos_eval_dates)

ind_date=mdl_1.dates>=oos_eval_dates(1)&mdl_1.dates<=oos_eval_dates(2);
mdl_1.returns = mdl_1.returns(ind_date,:);
mdl_1.dates=mdl_1.dates(ind_date);
mdl_1.T = numel(mdl_1.dates);
mdl_1.Tinv = 1.0 / mdl_1.T; 

[oos_ssdf,oos_dates]=oos_sdf(mdl_1,wnd_train,wnd_test);

% first step
%oos_ssdf=-oos_ssdf;
oos_ind=mdl_1.dates>=oos_dates(1)& mdl_1.dates<=oos_dates(end);
oos_exret=mdl_1.returns(oos_ind,mdl_1.Ns+1:end)-mdl_1.returns(oos_ind,1);
beta=(oos_ssdf'*oos_exret/(oos_ssdf'*oos_ssdf))';

% second step
av_exret=mean(oos_exret,1)';
%lam=beta'*av_exret/(beta'*beta);

lmd=fitlm(beta,av_exret);%,'Intercept',false)

end

