clc; clear all; close all;
set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
%cvx_setup;%load cvx

mdl.fpath = 'RF_FF3_Inter.mat';
mdl.Ns = 3;
mdl.div='jh';
mdl.shrink=false;



%% first model

mdl_1=mdl;
mdl_1.APT=true;
mdl_1.PCA=false;
mdl_1.taus=100;

mdl_1.pen='l2';
%mdl_1.pen = 'np';

%taumax_func(mdl_1);
mdl_1 = ssdf_setup(mdl_1);


oos_eval_dates=[datetime(1963,07,01),datetime(1993,7,01)];
ind_date=mdl_1.dates>=oos_eval_dates(1)&mdl_1.dates<=oos_eval_dates(2);
%dreturns=mdl_1.returns(ind_date,2:end);

mdl_1.returns = mdl_1.returns(ind_date,:);
%mdl_1.returns = mdl_1.returns(ind_date,1);
%mdl_1.N=mdl_1.Ns;
%mdl_1.Nd=0;

mdl_1.dates=mdl_1.dates(ind_date);

mdl_1.T = numel(mdl_1.dates);%#####
mdl_1.Tinv = 1.0 / mdl_1.T; % set inverse of T


res_1 = ssdf_run(mdl_1);

% pricerrs=ones(190,1) - mdl_1.Tinv * dreturns.' *res_1.ssdf;
% tauu=norm(pricerrs,2);
tauu=norm(mdl_1.rsSig\res_1.pricerrs(mdl_1.Ns+1:end),2);

%% second model

% mdl_2=mdl;
% mdl_2.APT=true;
% mdl_2.PCA=false;
% mdl.shrink=false;
% 
% 
% mdl_2.taus=100;
% 
% mdl_2.pen = 'l2';
% 
% 
% mdl_2 = ssdf_setup(mdl_2,datetime(1963,07,01),datetime(1993,7,01));
% 
% % oos_eval_dates=[datetime(1963,07,01),datetime(1993,7,01)];
% % ind_date=mdl_2.dates>=oos_eval_dates(1)&mdl_2.dates<=oos_eval_dates(2);
% % 
% % mdl_2.returns = mdl_2.returns(ind_date,:);
% % mdl_2.dates=mdl_2.dates(ind_date);
% % 
% % mdl_2.T = numel(mdl_2.dates);%#####
% % mdl_2.Tinv = 1.0 / mdl_2.T; % set inverse of T
% 
% res_2 = ssdf_run(mdl_2);
% tauu2=norm(mdl_2.rsSig\res_2.pricerrs(mdl_2.Ns+1:end),2);


mdl_2=mdl;
mdl_2.APT=true;
mdl_2.PCA=false;
mdl_2.taus=.18;
mdl_2.N_apt=2;

mdl_2.pen = 'l1';


mdl_2 = ssdf_setup(mdl_2);
res_2 = ssdf_run(mdl_2);
norm(mdl_2.rsSig\res_2.pricerrs(mdl_2.Ns+1:end),1);
%% third model

mdl_3=mdl;
mdl_3.APT=true;
mdl_3.PCA=false;
mdl_3.taus=.18;
mdl_3.N_apt=2;

mdl_3.pen = 'l1';


mdl_3 = ssdf_setup(mdl_3);
res_3 = ssdf_run(mdl_3);

%%
%model_plots2(mdl_1,mdl_2,res_1,res_2)
model_plots3(mdl_1,mdl_2,mdl_3,res_1,res_2,res_3)

%%
div_1=res_1.ssdf_div;
div_2=res_2.ssdf_div;
div_3=res_3.ssdf_div;


fprintf('\ndispersion  model 1 : %d\n',div_1);
fprintf('\ndispersion  model 2 : %d\n',div_2);
fprintf('\ndispersion  model 3 : %d\n',div_3);


if mdl_1.APT==true
    nor_1=norm(mdl_1.rsSig\res_1.pricerrs(mdl_1.Ns+1:end),2);
    fprintf('\npricing error:%d\n',nor_1)
else 
    nor_1=norm(res_1.pricerrs(mdl_1.Ns+1:end),inf);
    fprintf('\npricing error:%d\n',nor_1)
end

if mdl_2.APT==true
    nor_2=norm(mdl_2.rsSig\res_2.pricerrs(mdl_2.Ns+1:end),2);
    fprintf('\npricing error:%d\n',nor_2)
else 
    nor_2=norm(res_2.pricerrs(mdl_2.Ns+1:end),inf);
    fprintf('\npricing error:%d\n',nor_2)
end

if mdl_3.APT==true
    nor_3=norm(mdl_3.rsSig\res_3.pricerrs(mdl_3.Ns+1:end),2);
    fprintf('\npricing error:%d\n',nor_3)
else 
    nor_3=norm(res_3.pricerrs(mdl_3.Ns+1:end),inf);
    fprintf('\npricing error:%d\n',nor_3)
end

%%
