clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
% cvx_setup;%load cvx

mdl.fpath = 'RF_FF3_Inter.mat';
mdl.Ns = 1;
mdl.div = 'jh';

mdl.APT=true;
mdl.PCA=false;
mdl.shrink=true;
mdl.N_apt=1;

mdl2=mdl;
mdl2=ssdf_setup(mdl2);

oos_eval_dates=[datetime(1933,07,01),datetime(2018,07,30)];
ind_date=mdl2.dates>=oos_eval_dates(1)&mdl2.dates<=oos_eval_dates(2);

dates=mdl2.dates(ind_date);

wnd_train=12*30;
wnd_test=12;

loo=floor((length(dates)-wnd_train)/wnd_test);
taumax=zeros(loo+1,3);



%%
for i = 1:loo+1
    i

    bot = (i-1)*wnd_test+1;
    top = (i-1)*wnd_test+wnd_train;
    
    date_init=dates(bot);
    date_end=dates(top);
    
    mdl1=mdl;
    mdl1.pen='l1';
    mdl1.taus=100;    
    mdl1=ssdf_setup(mdl1,date_init,date_end);
    taumax(i,1)= taumax_func(mdl1);

    mdl1=mdl;
    mdl1.pen='l2';
    mdl1.taus=100;    
    mdl1=ssdf_setup(mdl1,date_init,date_end);
    taumax(i,2)= taumax_func(mdl1);

    mdl1=mdl;
    mdl1.pen='li';
    mdl1.taus=100;    
    mdl1=ssdf_setup(mdl1,date_init,date_end);
    taumax(i,3)= taumax_func(mdl1);

end

