clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')
%%
%load('JH_Rf_L1_noAPT_Inter_wIntercept_20190201_0207.mat')  % L1
%load('JH_Rf_L2_noAPT_Inter_wIntercept_20190201_0227.mat')   % L2
%load('JH_Rf_Linf_noAPT_Inter_wIntercept_20190201_0248.mat') %Linf
%load('JH_Rf_L1_APT_Inter_wIntercept_20190201_1013.mat')% L1 APT
% load('JH_Rf_L2_APT_Inter_wIntercept_20190201_1035.mat') % L2 APT
%load('JH_Rf_Linf_APT_Inter_wIntercept_20190201_1058.mat') %LinfAPT
%%

%% oos Adj Rsquared, Constant, oos RMSE
figure(1)
subplot(3,1,1)
plot(Tau,RsqAdj,'LineWidth',2);hold on;
%plot([nor,nor],[min(RsqAdj)-1.05*abs(min(RsqAdj)),max(RsqAdj)*1.15],'--r','LineWidth',2.5);hold off;
xlabel('tau');
ylabel('oos Adj-R^2 ')
%ylim([min(RsqAdj)-1.05*abs(min(RsqAdj)),max(RsqAdj)*1.05])

subplot(3,1,2)
%bar(Tau,Const);hold on;
plot(Tau,GLSRsqAdj,'LineWidth',2)
%plot([nor,nor],[min(Const)-.05,max(Const)*1.15],'--r','LineWidth',2.5);hold off;
%ylim([min(Const)-1.05*abs(min(Const)),max(Const)*1.05])
xlabel('tau');
ylabel('GLS-Adj-R^2');

subplot(3,1,3)
plot(Tau,oos_pe_h,'LineWidth',2);
xlabel('tau');
ylabel('h(oos-PE)')

%%
% Out of Sample sSDF
[a,b]=min(Const);
[c,d]=max(RsqAdj);
figure(2)

subplot(3,1,1)
plot_sdf_oos(sSDF(:,1),dat(:,1))
subplot(3,1,2)
plot_sdf_oos(sSDF(:,b),dat(:,b))
subplot(3,1,3)
plot_sdf_oos(sSDF(:,d),dat(:,d))



%%
% In Sample Pricing Error
figure(3)

subplot(3,1,1)
colormap('parula');
%colormap gray;
imagesc(reshape(in_price_er(:,:,1),380,18)')
colorbar;
subplot(3,1,2)
imagesc(reshape(in_price_er(:,:,b),380,18)')
colorbar;
subplot(3,1,3)
imagesc(reshape(in_price_er(:,:,d),380,18)')
colorbar;

%suptitle('In Sample Pricing Error')

%%
% In Sample Weight
figure(4)

subplot(3,1,1)
colormap('parula');
imagesc(reshape(in_opt_we(:,:,1),380,18)')
colorbar;
subplot(3,1,2)
imagesc(reshape(in_opt_we(:,:,b),380,18)')
colorbar;
subplot(3,1,3)
imagesc(reshape(in_opt_we(:,:,d),380,18)')
colorbar;

%suptitle('In Sample Optimal Weight')