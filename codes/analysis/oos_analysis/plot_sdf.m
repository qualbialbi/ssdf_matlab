function plot_sdf(mdl,x,c)

init_date_sdf=datetime(1993,07,01);

% reccession periods
rd1_i=datetime(2001,03,01);
rd1_f=datetime(2001,11,01);

rd2_i=datetime(2007,12,01);
rd2_f=datetime(2009,06,01);

x_points = [rd1_i, rd1_i, rd1_f, rd1_f];  
x_points = [x_points;[rd2_i, rd2_i, rd2_f, rd2_f]];  

d=mdl.dates>init_date_sdf;
b=(max(x(d))-min(x(d)))*.2;
plot(mdl.dates(d),x(d),c);title('sdf')
y_points = [min(x(d))-b, max(x(d))+b, max(x(d))+b, min(x(d))-b];hold on;
for i=1:2
a = fill(x_points(i,:), y_points, [0.3 0.3 0.3],'LineStyle','none');a.FaceAlpha = 0.2;
end
ylim([min(x(d))-b max(x(d))+b]);hold off;
title('sdf')
end