clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
cvx_setup;%load cvx
mdl.fpath = 'RF_FF3_Inter.mat';
%mdl.av=1;
mdl.Ns = 1;
mdl.div = 'jh';
mdl.pen = 'l2';
mdl.APT=false;
mdl.PCA=false;
mdl.shrink=false;
%mdl.N_apt=1;

load('taumax_jh_high3318_1.mat');
%nor=max(taumax(:,1))*1.10;
mdl.taumax=taumax(:,2);

oos_eval_dates=[datetime(1933,07,01),datetime(1964,06,30)];
wnd_train=12*30;
wnd_test=12;
Tau_rat=linspace(0,1.25,3);
taustar=mdl.taumax*Tau_rat;

%%
RsqAdj=zeros(size(Tau_rat));
RMSE=zeros(size(Tau_rat));
Const=zeros(size(Tau_rat));
Lambd=zeros(size(Tau_rat));
Tst=zeros(2,length(Tau_rat));
Pv=zeros(2,length(Tau_rat));

GLSRsqAdj=zeros(size(Tau_rat));
GLSRMSE=zeros(size(Tau_rat));
GLSConst=zeros(size(Tau_rat));
GLSLambd=zeros(size(Tau_rat));
GLSTst=zeros(2,length(Tau_rat));
GLSPv=zeros(2,length(Tau_rat));

sSDF=zeros(660,length(Tau_rat));
dat = NaT(660,length(Tau_rat));
in_price_er=zeros(188,1,length(Tau_rat));
in_opt_we=zeros(188,1,length(Tau_rat));
out_price_er=zeros(188,1,length(Tau_rat));
in_price_er_h=zeros(length(Tau_rat),1);
oos_pe=zeros(188,length(Tau_rat));
oos_pe_h=zeros(length(Tau_rat),1);


for i=1:length(Tau_rat)
    mdl_1=mdl;
    mdl_1.tau_rat=.2;
    %mdl_1.taus=1;
    
    %mdl_1 = ssdf_setup(mdl_1);
    oos_albi = oos_fm(mdl_1,wnd_train,wnd_test,oos_eval_dates);
    mdl_1=mdl;
    mdl_1.tau_rat=.2;
    [oos_ssdf,oos_dates,lmd,lmd1,in_price_er(:,:,i),in_opt_we(:,:,i),in_price_er_h(i),oos_pe(:,i)]...
        =oos_fm(mdl_1,wnd_train,wnd_test,oos_eval_dates);

    Const(i)=table2array(lmd.Coefficients(1,1));
    Lambd(i)=table2array(lmd.Coefficients(2,1));
    RsqAdj(i)=lmd.Rsquared.Adjusted;
    Tst(:,i)=table2array(lmd.Coefficients(:,3));
    Pv(:,i)=table2array(lmd.Coefficients(:,4));
    RMSE(i)=lmd.RMSE;

    GLSConst(i)=table2array(lmd1.Coefficients(1,1));
    GLSLambd(i)=table2array(lmd1.Coefficients(2,1));
    GLSRsqAdj(i)=lmd1.Rsquared.Adjusted;
    GLSTst(:,i)=table2array(lmd1.Coefficients(:,3));
    GLSPv(:,i)=table2array(lmd1.Coefficients(:,4));
    GLSRMSE(i)=lmd1.RMSE;

    sSDF(:,i)=oos_ssdf;
    dat(:,i)=oos_dates; 

    oos_pe_h(i)=norm(oos_pe(:,i),1);

end
dat=datetime(dat,'InputFormat','yyyy mm','Format', 'yyyy MM');