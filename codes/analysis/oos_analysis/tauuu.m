   
clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
% cvx_setup;%load cvx

mdl.fpath = 'RF_FF3_Inter.mat';
mdl.Ns = 2;
mdl.div = 'jh';

mdl.APT=false;
mdl.PCA=false;
mdl.shrink=false;
mdl2=mdl;
mdl2=ssdf_setup(mdl2);

oos_eval_dates=[datetime(1933,07,01),datetime(2018,12,30)];
ind_date=mdl2.dates>=oos_eval_dates(1)&mdl2.dates<=oos_eval_dates(2);
mdl2.returns = mdl2.returns(ind_date,:);
mdl2.dates=mdl2.dates(ind_date);

mdl2.T = numel(mdl2.dates);%#####

wnd_train=12*30;
wnd_test=12;

loo=floor((mdl2.T-wnd_train)/wnd_test);
taumax=zeros(loo+1,3);


mdl.APT=true;
mdl.N_apt=1;
%%
for i = 1:loo+1
    i

    bot = (i-1)*wnd_test+1;
    top = (i-1)*wnd_test+wnd_train;

    mdl1=mdl;
    mdl1.returns = mdl2.returns( bot:top, : );
    mdl1.FF3= mdl2.FF3(bot:top,:);%#######
    mdl1.rf=mdl2.rf(bot:top);
    mdl1.dates=mdl2.dates( bot:top, : );
    mdl1.pen='l1';
    taumax(i,1)= taumax_func(mdl1);

    mdl1=mdl;
    mdl1.pen='l2';
    mdl1.returns = mdl2.returns( bot:top, : );
    mdl1.FF3= mdl2.FF3(bot:top,:);%#######
    mdl1.rf=mdl2.rf(bot:top);
    mdl1.dates=mdl2.dates( bot:top, : );

    taumax(i,2)= taumax_func(mdl1);

    mdl1=mdl;
    mdl1.pen='li';    
    mdl1.returns = mdl2.returns( bot:top, : );
    mdl1.FF3= mdl2.FF3(bot:top,:);%#######
    mdl1.rf=mdl2.rf(bot:top);
    mdl1.dates=mdl2.dates( bot:top, : );


    taumax(i,3)= taumax_func(mdl1);

end

