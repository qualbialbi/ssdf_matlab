%% OOS test

% add paths
paths_setup();
% setup cvx
cvx_setup;

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';
mdl.Ns = 4;
mdl.div = 'h';
mdl.pen = 'l';
mdl.lambdas = 0.001;
mdl.oos = true;
mdl = ssdf_setup(mdl);
pricerr = oos_run(mdl);