%% FF3F100P OOS Analysis

% add paths
paths_setup();
% setup cvx
cvx_setup;

% set mdl
mdl.fpath = 'FF3F100P_monthly.mat';

%% Hansen-Jagannathan
% all-sure
mdl.Ns = 104;
mdl.div = 'h';
mdl = ssdf_setup(mdl);
oos_FF3F100P.res_h_allsure = oos_run(mdl);

% l2
Nfact = 4;
Nlamb = 5;
oos_FF3F100P.res_h_l2 = zeros(Nlamb, Nfact);

for n = 0:Nfact
    for l = 1:Nlamb
        
        
    end
end
mdl.Ns = 4;
mdl.pen = 'r';
mdl.div = 'h';
mdl = ssdf_setup(mdl);
oos_FF3F.res_h = oos_run(mdl);