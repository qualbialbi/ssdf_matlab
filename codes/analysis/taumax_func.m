function taumax = taumax_func(mdl)
  
if ~any(strcmp(mdl.pen,{'li','cli','l1','l2','r2'}))
    taumax=0;
    return 
end

if any(strcmp(mdl.pen,{'li','cli'}))
    nor=inf;
end
if strcmp(mdl.pen,'l1')
    nor=1;
end
if any(strcmp(mdl.pen,{'l2','r2'}))
    nor=2;
end

mdl.taus=100;
mdl = ssdf_setup(mdl);
res = ssdf_run(mdl);

if mdl.APT==true
    taumax=norm(mdl.rsSig\res.pricerrs(mdl.Ns+1:end),nor);
else
    taumax=norm(res.pricerrs(mdl.Ns+1:end),nor);
end

end