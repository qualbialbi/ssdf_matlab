clc; clear all; close all;
%set(0,'DefaultFigureWindowStyle','docked')

%% Setup
paths_setup();
%cvx_setup;%load cvx

mdl.fpath = 'RF_FF3_Low.mat';
mdl.Ns = 1;

mdl.alpha=1e-3;
%mdl.lambda=0.05;
%%
mdl_1=mdl;
mdl_1.APT=false;
mdl_1.PCA=false;

mdl_1.taus=.01;

mdl_1.div = 'jh';
mdl_1.pen = 'li';

wnd_train=360;
wnd_test=12;

mdl_1 = ssdf_setup(mdl_1);

oos_eval_dates=[datetime(1963,07,01),datetime(2010,12,30)];
ind_date=mdl_1.dates>=oos_eval_dates(1)&mdl_1.dates<=oos_eval_dates(2);
mdl_1.returns = mdl_1.returns(ind_date,:);
mdl_1.dates=mdl_1.dates(ind_date);
mdl_1.T = numel(mdl_1.dates);
mdl_1.Tinv = 1.0 / mdl_1.T; 

[oos_ssdf,oos_dates]=oos_sdf(mdl_1,wnd_train,wnd_test);

%%

% first step
%oos_ssdf=-oos_ssdf;
oos_ind=mdl_1.dates>=oos_dates(1)& mdl_1.dates<=oos_dates(end);
oos_exret=mdl_1.returns(oos_ind,mdl_1.Ns+1:end)-mdl_1.returns(oos_ind,1);
beta=(oos_ssdf'*oos_exret/(oos_ssdf'*oos_ssdf))';

% second step
av_exret=mean(oos_exret,1)';
lam=beta'*av_exret/(beta'*beta);

md2=fitlm(beta,av_exret);%,'Intercept',false)

%md1=fitlm(oos_sdf,oos_exret)

%%
