function paths_setup()

    % add paths
    addpath('../functions/utils');
    addpath('../functions/optim/div');
    addpath('../functions/optim/pen');
    addpath('../functions/optim/pricerr');
    addpath('../functions/optim/ssdf_div');
    addpath('../functions/optim/link');
    addpath('../functions/optim/fminunc');
    addpath('../../data');
    addpath('../../external/cvx');

end