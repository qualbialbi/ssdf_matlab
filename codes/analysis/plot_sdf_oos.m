function plot_sdf_oos(oos_ssdf,oos_dates)

rd1_i=datetime(2001,03,01);
rd1_f=datetime(2001,11,01);

rd2_i=datetime(2007,12,01);
rd2_f=datetime(2009,06,01);

x_points = [rd1_i, rd1_i, rd1_f, rd1_f];  
x_points = [x_points;[rd2_i, rd2_i, rd2_f, rd2_f]];  


b=(max(oos_ssdf)-min(oos_ssdf))*.2;
plot(oos_dates,oos_ssdf)
y_points = [min(oos_ssdf)-b, max(oos_ssdf)+b, max(oos_ssdf)+b, min(oos_ssdf)-b];hold on;
for i=1:2
a = fill(x_points(i,:), y_points, [0.3 0.3 0.3],'LineStyle','none');a.FaceAlpha = 0.2;
end
ylim([min(oos_ssdf)-b max(oos_ssdf)+b]);hold off;
title('sdf')
end